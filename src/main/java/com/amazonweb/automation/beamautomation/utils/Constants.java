package com.amazonweb.automation.beamautomation.utils;

public class Constants {

	public static final String baseUrl = "https://www.amazon.com/";
	public static final String browserName = "Chrome";
	public static final String departmentName = "Computers";
	public static final String searchProductName = "Laptop";
}
