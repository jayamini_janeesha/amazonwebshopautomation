package com.amazonweb.automation.beamautomation.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtils {

	private static XSSFSheet ExcelSheet;
	private static XSSFWorkbook ExcelWorkbook;
	private static XSSFCell Cell;

	public static void setExcelSheet(String path, String SheetName) throws IOException {
		FileInputStream ExcelFile = new FileInputStream(path);
		ExcelWorkbook = new XSSFWorkbook(ExcelFile);
		ExcelSheet = ExcelWorkbook.getSheet(SheetName);

	}

	public static String getCellData(int rowNum, int colNum) {
		Cell = ExcelSheet.getRow(rowNum).getCell(colNum);
		String cellData = Cell.getStringCellValue();
		return cellData;
	}

	public static int getRowCount() {
		int rowCount = ExcelSheet.getLastRowNum() - ExcelSheet.getFirstRowNum();

		return rowCount;
	}

}
