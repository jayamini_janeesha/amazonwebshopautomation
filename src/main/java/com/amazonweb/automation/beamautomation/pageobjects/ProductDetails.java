package com.amazonweb.automation.beamautomation.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

public class ProductDetails extends BasePageObject {

	public ProductDetails(WebDriver driver) {
		super(driver);
	}


	@FindBy(id = "productTitle")
	private static WebElement eleProductNameHeading;

	@FindBy(id = "add-to-cart-button")
	private static WebElement eleAddToCartButton;
	
	@FindBy(xpath = "//div[@id='attach-added-to-cart-message']//h4")
	private static WebElement eleAddedToCartConfirmationAlertText;
	
	@FindBy(id = "nav-cart")
	private static WebElement eleShoppingCartLink;

	
	public static String getProductNameHeading() {
		
		String txtProductNameHeading = null;
		
		if (eleProductNameHeading.isDisplayed()) {
			txtProductNameHeading = eleProductNameHeading.getText();
			Reporter.log("Product Details Page: Found the product details heading");
		} else {
			Reporter.log("Product Details Page: ERROR Product details heading not found");
		}
		return txtProductNameHeading;

	}

	public static String getLinkURLOfProductPage() {

		String productPageURL = null;
		productPageURL = driver.getCurrentUrl();
		return productPageURL;

	}
	
	public static void clickOnAddToCartButton() throws InterruptedException {

		if (eleAddToCartButton.isDisplayed()) {
			eleAddToCartButton.click();
			Thread.sleep(5000);
			Reporter.log("Product Details Page: Found 'Add to Cart' button and clicked");
			
			try {
				if (driver.findElement(By.id("attach-close_sideSheet-link")).isDisplayed()) {
					driver.findElement(By.id("attach-close_sideSheet-link")).click();
					Thread.sleep(2000);
					
				}
				
			} catch (Exception e) {
				// TODO: handle exception
			}


		}
		else
		{
			Reporter.log("Product Details Page: Error 'Add to Cart' button not found");
		}

	}
	
	public static void clickOnShoppingCartLink() throws InterruptedException {

		if (eleShoppingCartLink.isDisplayed()) {
			eleShoppingCartLink.click();
			
			Reporter.log("Product Details Page: Found 'Add to Cart' button and clicked");

		}
		else
		{
			Reporter.log("Product Details Page: Error 'Add to Cart' button not found");
		}

	}
	


}
