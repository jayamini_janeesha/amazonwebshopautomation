package com.amazonweb.automation.beamautomation.pageobjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.amazonweb.automation.beamautomation.pageobjects.BasePageObject;

public class ShoppingCart extends BasePageObject {

	public ShoppingCart(WebDriver driver) {
		super(driver);
	}

	@FindBy(id = "nav-cart-count")
	private static WebElement eleShoppingCardItemCountLabel;

	@FindBy(xpath = "//div[@id='sc-active-cart']//div[@data-asin]")
	private static WebElement eleShoppingCartItemList;

	@FindBy(tagName = "h1")
	private static WebElement eleShoppingCartEmptyMessage;

	@FindBy(xpath = "//i[@class='a-icon a-icon-dropdown']")
	private static WebElement eleQuantityDropdownArrow;

	@FindBy(id = "dropdown1_9")
	private static WebElement eleQuantityListItem;

	@FindBy(xpath = "//div[@data-asin]//input[@type='text']")
	private static WebElement eleQuantityTextField;

	@FindBy(xpath = "//div[@data-asin]//a[@class='a-button-text']")
	private static WebElement eleUpdateQuantityButton;

	@FindBy(xpath = "//span[@id='sc-subtotal-amount-activecart']/span")
	private static WebElement eleSubTotalLabelBottom;

	@FindBy(xpath = "//div[@data-asin]//input[@value='Delete']")
	private static WebElement eleDeleteItemFromCartLink;

	public static boolean isProductToDeleteExist() {

		boolean found = false;
		if (eleDeleteItemFromCartLink.isDisplayed()) {
			found = true;
		} 
		
		return found;
	}
	
	public static void deleteAllItemsFromShoppingCart() throws InterruptedException {
		List<WebElement> shoppingCartItemsList= getListOfItemRows();
		for (int i = 0; i < shoppingCartItemsList.size(); i++) {
			if (shoppingCartItemsList.get(i) != null) {

				WebElement eleDeleteItemLink = shoppingCartItemsList.get(i).findElement(By.xpath("//div[@data-asin]//input[@value='Delete']"));
				if (eleDeleteItemLink.isDisplayed()) {
					eleDeleteItemLink.click();
					Thread.sleep(2000);
				} 
			}
		}


	}

	public static List<WebElement> getListOfItemRows() {

		List<WebElement> shoppingCartItemsList = driver.findElements(By.xpath("//div[@data-asin]"));
		return shoppingCartItemsList;

	}

	public static String getItemRowNameAttribute() {

		WebElement eleItemRow = driver.findElement(By.xpath("//div[@data-asin]"));
		return eleItemRow.getAttribute("data-asin");
	}

	public static String getItemNameAttributeInShoppingCart(String deleteItemRowName) {
		String actual = null;
		String formattedActual = null;

		WebElement eleItemRowDynamic = driver.findElement(By.xpath("//div[@data-asin='" + deleteItemRowName + "']"));
		actual = eleItemRowDynamic.getText();
		formattedActual = actual.substring(0, 20).trim();
		return formattedActual;
	}

	public static void deleteAnItemFromTheShoppingCartList() {

		if (eleDeleteItemFromCartLink.isDisplayed()) {
			eleDeleteItemFromCartLink.click();

		}

	}

	public static void selectItemQuantity(String quantity) {

		if (eleQuantityDropdownArrow.isEnabled()) {
			Actions action = new Actions(driver);
			action.moveToElement(eleQuantityDropdownArrow).click().build().perform();
			action.moveToElement(eleQuantityListItem).click().build().perform();

			if (eleQuantityTextField.isEnabled()) {
				eleQuantityTextField.sendKeys(quantity);

			}

		}

	}

	public static void clickOnUpdateQuantityButton() {

		if (eleUpdateQuantityButton.isEnabled()) {
			eleUpdateQuantityButton.click();

		}

	}

	public static String getCartSubTotalAmount() {

		String actual = null;
		String formattedActual = null;
		if (eleSubTotalLabelBottom.isDisplayed()) {
			actual = eleSubTotalLabelBottom.getText();
			// Getting the part after currency. this will work with any currency
			// based on user's browser settings or regional settings.
			String[] split = actual.split("\\s+");
			formattedActual = split[1].trim();
		}

		return formattedActual;

	}

	public static boolean verifyPageLoaded() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("h1")));

		boolean found = false;

		if (eleShoppingCartEmptyMessage.isDisplayed()) {
			found = true;
			Reporter.log("Shopping Cart Page: Shopping Cart Page loaded");

		} else {
			Reporter.log("Shopping Cart Page: ERROR Shopping Cart does load");
		}

		return found;
	}

	public static String verifyShoppingCartEmptyMessage() {
		String actualMessage = null;
		if (eleShoppingCartEmptyMessage.isDisplayed()) {
			actualMessage = eleShoppingCartEmptyMessage.getText();
		}

		return actualMessage;
	}

	public static String getShoppingCartItemCount() {

		String txtProductNameHeading = null;

		if (eleShoppingCardItemCountLabel.isDisplayed()) {
			txtProductNameHeading = eleShoppingCardItemCountLabel.getText();
			Reporter.log("Shopping Cart Page: Found shopping card label");
		} else {
			Reporter.log("Shopping Cart Page: ERROR  shopping card label not found");
		}
		return txtProductNameHeading;

	}

	public static boolean verifyDataExistsInShoppingCartList() {
		boolean found = false;

		if (eleShoppingCartItemList.isDisplayed()) {
			found = true;
			Reporter.log("Shopping Cart Page: Shopping Cart List exists");

		} else {
			Reporter.log("Shopping Cart Page: ERROR Shopping Cart does not exist");
		}

		return found;
	}

}
