package com.amazonweb.automation.beamautomation.pageobjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Reporter;

public class SearchResultsList extends BasePageObject{

	public SearchResultsList(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(xpath = "//div[@data-asin]//h5/a")
	private static WebElement eleSearchResultItem;

	public static List<String> printSearchResultNames() {

		List<String> searchResultsList = new ArrayList<String>();

		List<WebElement> searchResultsEleList = driver.findElements(By.xpath("//div[@data-asin]//h5//span"));
		int count = searchResultsEleList.size();
		for (int i = 0; i < count; i++) {
			searchResultsList.add(searchResultsEleList.get(i).getText());
		}
		return searchResultsList;

	}

	public static void clickOnProductLinkFromSearchResults() {

		if (eleSearchResultItem.isDisplayed()) {
			eleSearchResultItem.click();
			Reporter.log("Search Results List Page: Found results and clicked on a product link");
		}
		else
		{
			Reporter.log("Search Results List Page: Search results list not found");
		}

	}
	
	public static String getLinkURLOfOneItemInSearchResultsList() {

		String productNameLinkURL = null;
		if (eleSearchResultItem.isDisplayed()) {
			productNameLinkURL = eleSearchResultItem.getAttribute("href");
		}

		return productNameLinkURL;


	}
	
}
