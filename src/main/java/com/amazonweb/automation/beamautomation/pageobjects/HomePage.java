package com.amazonweb.automation.beamautomation.pageobjects;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.enclosing.WelzlEncloser;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

public class HomePage extends BasePageObject {

	public HomePage(WebDriver driver) {
		super(driver);
	}

	@FindBy(css = "a[class='nav-logo-link']")
	private static WebElement eleHomePageLogo;

	@FindBy(id = "nav-shop")
	private static WebElement eleAllDepartmentsList;

	@FindBy(xpath = "//a[@id='nav-link-shopall']//span[@class='nav-icon nav-arrow']")
	private static WebElement eleAllDepartmentsListArrow;

	@FindBy(xpath = "//span[@id='searchDropdownDescription']/..")
	private static WebElement eleSearchDepartmentDropdownSpan;

	@FindBy(id = "searchDropdownBox")
	private static WebElement eleSearchDepartmentDropdownList;

	@FindBy(id = "twotabsearchtextbox")
	private static WebElement eleSearchTextbox;

	@FindBy(css = "input[type='submit']")
	private static WebElement eleSearchSubmitButton;
	// span[@data-component-type="s-search-results"]//div[@data-asin]//h5//span

	@FindBy(xpath = "//input[@class='a-button-input']")
	private static WebElement eleLocationConfimeAlertOKButton;



	//
	public static boolean verifyLogoExists() {
		boolean found = false;

		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("a[class='nav-logo-link']")));

		if (eleHomePageLogo.isDisplayed()) {
			found = true;
			Reporter.log("Home Page: Logo exists");

		} else {
			Reporter.log("Home Page: Logo does not exist");
		}

		return found;
	}

	public static void clickOnLogo() {

		if (eleHomePageLogo.isDisplayed()) {
			eleHomePageLogo.click();
		}

	}

	public static boolean verifyAllDepartmentListExists() {
		boolean found = false;

		if (eleAllDepartmentsList.isDisplayed()) {
			found = true;
			Reporter.log("Home Page: All Departments list exists");

		} else {
			Reporter.log("Home Page: All Departments list does not exist");
		}

		return found;
	}

	public static void hoverOverDepartmentsList() throws InterruptedException {

		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(By.id("nav-shop"))).perform();
		action.moveToElement(driver.findElement(By.id("nav-link-shopall"))).build().perform();

		Thread.sleep(2000);

	}

	public static void printAllDepartmentNames() {

		List<String> menuTitles = new ArrayList<String>();

		List<WebElement> menuList = driver.findElements(By.xpath("//div[@id='nav-flyout-shopAll']//a//span"));
		int count = menuList.size();
		for (int i = 0; i < count; i++) {
			menuTitles.add(menuList.get(i).getText());
			Reporter.log(menuTitles.get(i));
		}

	}

	public static Boolean isLocationConfirmAlertExists() {

		boolean found = false;

		if (eleLocationConfimeAlertOKButton.isDisplayed()) {
			found = true;
		}

		return found;

	}

	public static void handleLocationConfirmAlert() {

		eleLocationConfimeAlertOKButton.click();

	}

	public static void selectSearchDepartment(String departmentName) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@id='searchDropdownDescription']/..")));

		eleSearchDepartmentDropdownSpan.click();
		Select department = new Select(eleSearchDepartmentDropdownList);
		department.selectByVisibleText(departmentName);

	}

	public static void enterSearchCriteria(String searchText) {

		if (eleSearchTextbox.isEnabled()) {
			eleSearchTextbox.sendKeys(searchText);
			Reporter.log("Home Page: Entered Search Text");
		} else {
			Reporter.log("Home Page: Search Text field does not exist");
		}
	}

	public static void clickOnSearchButton() {

		if (eleSearchSubmitButton.isDisplayed()) {
			eleSearchSubmitButton.click();
			Reporter.log("Home Page: Clicked on Search products button");
		} else {
			Reporter.log("Home Page: Search products button does not exist");
		}

	}
	

}
