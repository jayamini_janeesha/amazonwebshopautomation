package com.amazonweb.automation.beamautomation;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.amazonweb.automation.beamautomation.pageobjects.HomePage;
import com.amazonweb.automation.beamautomation.pageobjects.ProductDetails;
import com.amazonweb.automation.beamautomation.pageobjects.SearchResultsList;
import com.amazonweb.automation.beamautomation.pageobjects.ShoppingCart;
import com.amazonweb.automation.beamautomation.utils.Constants;

public class AmazonWebTestCases extends AmazonWebTestBase {

	//SoftAssert sAssert = new SoftAssert();
	public String departmentName = Constants.departmentName;
	public String searchProductName = Constants.searchProductName;

	
	  @Test(priority=1)
	  //To sync until the home page is loaded and verify it the logo and
	  //All Department list elements are displayed 
	  public void verifyElementsExistOnHomePage() throws InterruptedException {
		  SoftAssert sAssert = new SoftAssert();
		  PageFactory.initElements(driver, HomePage.class);
		  sAssert.assertTrue(HomePage.verifyLogoExists());
		  sAssert.assertTrue(HomePage.verifyAllDepartmentListExists());
		  sAssert.assertAll(); HomePage.hoverOverDepartmentsList();
		  HomePage.printAllDepartmentNames();
	 }
	 

	@Test(priority=2)
	// Search for a product and verify the search result list is contains the product name searched for
	public void searchForItem() throws InterruptedException {
		SoftAssert sAssert = new SoftAssert();
		List<String> searchResultTextList = new ArrayList<String>();
		PageFactory.initElements(driver, HomePage.class);
		HomePage.selectSearchDepartment(departmentName);
		HomePage.enterSearchCriteria(searchProductName);
		HomePage.clickOnSearchButton();

		PageFactory.initElements(driver, SearchResultsList.class);
		searchResultTextList = SearchResultsList.printSearchResultNames();


			for (int j = 0; j < searchResultTextList.size(); j++) {
				String name = searchResultTextList.get(j);
				sAssert.assertTrue(name.contains(searchProductName),
							"Search result validation failed at row  :" + j +"   "+ name);
	
		} 

	}
	
	@Test(priority=3)
	// Search for a product and click on an item link on the results list to navigate to Product Details page
	//Verify that the page title is not empty
	public void navigateToProductsDetailPage() throws InterruptedException {
		SoftAssert sAssert = new SoftAssert();
		String productNameHeadingText = null;
		
		PageFactory.initElements(driver, HomePage.class);
		HomePage.selectSearchDepartment(departmentName);
		HomePage.enterSearchCriteria(searchProductName);
		HomePage.clickOnSearchButton();
		PageFactory.initElements(driver, SearchResultsList.class);
		SearchResultsList.clickOnProductLinkFromSearchResults();
		PageFactory.initElements(driver, ProductDetails.class);
		productNameHeadingText = ProductDetails.getProductNameHeading().trim();
		sAssert.assertTrue(!productNameHeadingText.isEmpty());
		sAssert.assertAll();

	}
	
	
	@Test(priority=4)
	// Search for a product and click on an item link on the results list
	public void selectAnItemFromSearchResultsAndVerifyRedirectCorrectPage() throws InterruptedException {
		SoftAssert sAssert = new SoftAssert();
		String searchResultListItemURL = null;
		String itemPageURL = null;
		
		PageFactory.initElements(driver, HomePage.class);
		HomePage.selectSearchDepartment(departmentName);
		HomePage.enterSearchCriteria(searchProductName);
		HomePage.clickOnSearchButton();
		PageFactory.initElements(driver, SearchResultsList.class);
		searchResultListItemURL= SearchResultsList.getLinkURLOfOneItemInSearchResultsList();
		SearchResultsList.clickOnProductLinkFromSearchResults();
		PageFactory.initElements(driver, ProductDetails.class);
		itemPageURL = ProductDetails.getLinkURLOfProductPage();
		sAssert.assertTrue(searchResultListItemURL.trim().equalsIgnoreCase(itemPageURL.trim()));
		sAssert.assertAll();

	}
	
	@Test(priority=5)
	// Verify when user does not select any items, the shopping cart page is empty
	public void verifyShoppingCartIsEmpty() throws InterruptedException {
		SoftAssert sAssert = new SoftAssert();
		PageFactory.initElements(driver, ProductDetails.class);
		ProductDetails.clickOnShoppingCartLink();
		PageFactory.initElements(driver, ShoppingCart.class);
		ShoppingCart.verifyPageLoaded();
		String itemCount = ShoppingCart.getShoppingCartItemCount();
		sAssert.assertEquals(itemCount,"0");
		String actualMessageText = ShoppingCart.verifyShoppingCartEmptyMessage();
		sAssert.assertTrue(actualMessageText.contains("empty"));
		sAssert.assertAll();

	}
	
	@Test(priority=6)
	// Search for a product, add to cart and verify that the shopping cart contains products
	public void addAnItemToCartAndVerifyCartPropertiesChange() throws InterruptedException {
		SoftAssert sAssert = new SoftAssert();
		int countBefore = 0;
		int countAfter = 0;
		String itemCount = null;
		
		//Check the number of items in the shopping cart before adding items
		PageFactory.initElements(driver, ProductDetails.class);
		ProductDetails.clickOnShoppingCartLink();
		PageFactory.initElements(driver, ShoppingCart.class);
		ShoppingCart.verifyPageLoaded();
		itemCount = ShoppingCart.getShoppingCartItemCount();
		countBefore = Integer.parseInt(itemCount);
		
		//Search for an item
		PageFactory.initElements(driver, HomePage.class);
		HomePage.selectSearchDepartment(departmentName);
		HomePage.enterSearchCriteria(searchProductName);
		HomePage.clickOnSearchButton();
		
		//Select a product to view details
		PageFactory.initElements(driver, SearchResultsList.class);
		SearchResultsList.clickOnProductLinkFromSearchResults();
		PageFactory.initElements(driver, ProductDetails.class);
		ProductDetails.clickOnAddToCartButton();
		
		//Add to shopping cart
		ProductDetails.clickOnShoppingCartLink();
		PageFactory.initElements(driver, ShoppingCart.class);
		itemCount = ShoppingCart.getShoppingCartItemCount();
		countAfter = Integer.parseInt(itemCount);
		sAssert.assertTrue(countAfter==countBefore+1);

		sAssert.assertAll();

	}

	@Test(priority=7)
	// Increase the quantity of an item in the shopping cart page and verify the total amount is changed
	public void increaseItemCountAndVerifyPriceChange() throws InterruptedException {
		SoftAssert sAssert = new SoftAssert();
		String amountAfter = null;
		String amountBefore = null;
		
		//Search for an item
		PageFactory.initElements(driver, HomePage.class);
		HomePage.selectSearchDepartment(departmentName);
		HomePage.enterSearchCriteria("Mouse");
		HomePage.clickOnSearchButton();
		
		//Select a product to view details
		PageFactory.initElements(driver, SearchResultsList.class);
		SearchResultsList.clickOnProductLinkFromSearchResults();
		PageFactory.initElements(driver, ProductDetails.class);
		ProductDetails.clickOnAddToCartButton();
		
		//Add to shopping cart
		ProductDetails.clickOnShoppingCartLink();
		PageFactory.initElements(driver, ShoppingCart.class);
		
		//Getting the Sub total amount before increasing the quantity
		amountBefore = ShoppingCart.getCartSubTotalAmount();
		
		//Increase the quantity
		ShoppingCart.selectItemQuantity("10");
		ShoppingCart.clickOnUpdateQuantityButton();
		Thread.sleep(3000);
		amountAfter = ShoppingCart.getCartSubTotalAmount();
		sAssert.assertNotEquals(amountAfter, amountBefore);

		sAssert.assertAll();

	}
		
	@Test(priority=8)
	// Add second product and verify the shopping cart properties change
	public void addItemsAndVerifyCardPropertiesChange() throws InterruptedException {
		SoftAssert sAssert = new SoftAssert();
		//Search for an item
		PageFactory.initElements(driver, HomePage.class);
		HomePage.selectSearchDepartment(departmentName);
		HomePage.enterSearchCriteria("smart tv");
		HomePage.clickOnSearchButton();
		
		//Select a product to view details
		PageFactory.initElements(driver, SearchResultsList.class);
		SearchResultsList.clickOnProductLinkFromSearchResults();
		PageFactory.initElements(driver, ProductDetails.class);
		ProductDetails.clickOnAddToCartButton();
		
		//Add to shopping cart
		ProductDetails.clickOnShoppingCartLink();
		PageFactory.initElements(driver, ShoppingCart.class);
		sAssert.assertEquals(ShoppingCart.getShoppingCartItemCount(), "1");
		
		//Add the second item
		HomePage.selectSearchDepartment(departmentName);
		HomePage.enterSearchCriteria("Mouse");
		HomePage.clickOnSearchButton();
		SearchResultsList.clickOnProductLinkFromSearchResults();
		ProductDetails.clickOnAddToCartButton();
		ProductDetails.clickOnShoppingCartLink();
		
		//Verify the number of rows equals to two, and the cart item count is two
		sAssert.assertEquals(ShoppingCart.getShoppingCartItemCount(), "2");
		
		List<WebElement> shoppingCartItemsList = ShoppingCart.getListOfItemRows();
		sAssert.assertEquals(shoppingCartItemsList.size(), 2);
		
		sAssert.assertAll();

	}
	
	@Test (priority=9)
	// Delete an item from shopping cart and verify it's being removed
	public void deleteProductFromCartAndVerifyPriceChange() throws InterruptedException {
		SoftAssert sAssert = new SoftAssert();
		String itemRowName = null;
		//Search for an item
		PageFactory.initElements(driver, HomePage.class);
		HomePage.selectSearchDepartment(departmentName);
		HomePage.enterSearchCriteria("smart tv");
		HomePage.clickOnSearchButton();
		
		//Select a product to view details
		PageFactory.initElements(driver, SearchResultsList.class);
		SearchResultsList.clickOnProductLinkFromSearchResults();
		PageFactory.initElements(driver, ProductDetails.class);
		ProductDetails.clickOnAddToCartButton();
		
		//Add to shopping cart
		ProductDetails.clickOnShoppingCartLink();
		PageFactory.initElements(driver, ShoppingCart.class);
		
		//Add the second item
		HomePage.selectSearchDepartment(departmentName);
		HomePage.enterSearchCriteria("Mouse");
		HomePage.clickOnSearchButton();
		SearchResultsList.clickOnProductLinkFromSearchResults();
		ProductDetails.clickOnAddToCartButton();
		ProductDetails.clickOnShoppingCartLink();
		
		//Delete the first product in the list
		itemRowName = ShoppingCart.getItemRowNameAttribute();
		String strBeforeDeleteName = ShoppingCart.getItemNameAttributeInShoppingCart(itemRowName.trim());
		
		ShoppingCart.deleteAnItemFromTheShoppingCartList();
		itemRowName = ShoppingCart.getItemRowNameAttribute();
		String strAfterDeleteName = ShoppingCart.getItemNameAttributeInShoppingCart(itemRowName.trim());
		sAssert.assertTrue(strBeforeDeleteName.equalsIgnoreCase(strAfterDeleteName));

		sAssert.assertAll();

	}
}
