package com.amazonweb.automation.beamautomation;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.asserts.SoftAssert;

import com.amazonweb.automation.beamautomation.pageobjects.BasePageObject;
import com.amazonweb.automation.beamautomation.pageobjects.HomePage;
import com.amazonweb.automation.beamautomation.pageobjects.ProductDetails;
import com.amazonweb.automation.beamautomation.pageobjects.ShoppingCart;
import com.amazonweb.automation.beamautomation.utils.Constants;


public class AmazonWebTestBase {

	public WebDriver driver;
	public String testDataFilePath = "./TestData/TestData.xlsx";
	public String sTestCaseName;


	@BeforeSuite
	public void beforeSuite() throws InterruptedException, IOException {
		String browserName = Constants.browserName;
		String baseUrl = Constants.baseUrl.trim();

		if (browserName.equals("Chrome")) {
			System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
			driver = new ChromeDriver();

		} else if (browserName.equals("Mozilla")) {

			System.setProperty("webdriver.gecko.driver", "./Drivers/geckodriver.exe");
			driver = new FirefoxDriver();

		}

		driver.get(baseUrl);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		new BasePageObject(driver);
		Thread.sleep(2000);
	}
	
	@BeforeMethod
	public void beforeMethod(Method method)
	{
		sTestCaseName = method.getName();
		Reporter.log("------------" + sTestCaseName + "----------");
		System.out.println("------------" + sTestCaseName + "----------");
		
		PageFactory.initElements(driver, HomePage.class);
		try {
			if (HomePage.isLocationConfirmAlertExists()) {
				HomePage.handleLocationConfirmAlert();
				
			}
			
		} catch (Exception e) {

		}

		
	}
	
	@AfterMethod
	public void afterMethod() throws InterruptedException
	{
		int itemsCount = 0;
		try {
			//Delete shopping cart content if there are any items in the shopping cart
			PageFactory.initElements(driver, ProductDetails.class);
			ProductDetails.clickOnShoppingCartLink();
			PageFactory.initElements(driver, ShoppingCart.class);
			itemsCount = Integer.parseInt(ShoppingCart.getShoppingCartItemCount());
			if (itemsCount>0) {
				ShoppingCart.deleteAllItemsFromShoppingCart();
				
			}
		} catch (Exception e) {
			Reporter.log(e.toString());
		}
		
		PageFactory.initElements(driver, HomePage.class);
		HomePage.clickOnLogo();
		Thread.sleep(3000);
	}

	@AfterSuite
	public void afterSuite() {
		driver.quit();
	}

}
